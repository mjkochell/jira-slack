require('dotenv').config()
const slackbot = require('./src/slackbot')
const controllerModule = require('./src/controller')

const main = async() => {
  const {bot, controller, res} = await slackbot.init()
  controllerModule.run(controller)
}

main()
