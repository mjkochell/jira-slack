const slackbot = require('./slackbot')
const JiraClient = require('./jira')
const Thread = require('./slack-threads')
const slackStore = require('./slack-store')
const sayings = require('../sayings')
const {JIRA_HOST, JIRA_PROJECT} = process.env

const fs = require('fs');
const saveSayings = (data, callback) => fs.writeFileSync('sayings.json', JSON.stringify(data, null, 2), 'utf8', callback);

const specificCategories = ['error']
const getRandomResponse = (name) => {
  let items = sayings[name]
  if (specificCategories.indexOf(name) === -1) {
    items = items.concat(sayings.generic)
  }
  return items[Math.floor(Math.random() * items.length)]
}
const jiraClient = new JiraClient()

const getErrorReply = e => {
  let errorMsg
  try {
    errorMsg = JSON.stringify(e)
    if(errorMsg === '{}') {
      errorMsg = e.message
    }
  }
  catch (e) {
    errorMsg = e.message || `No error message, whoops.`
  }
  return `${getRandomResponse('error')} ${errorMsg}`
}

const handleIssueCommand = async (controller) => {
  controller.hears(['^create-issue'], ['direct_mention', 'ambient'], async(bot, message) => {
    try {
      bot.replyAcknowledge()
      const project = jiraClient.info.projects[JIRA_PROJECT]
      const taskIssueType = jiraClient.info.issueTypes.Task
      const name = message.text.split(' ').slice(1).join(' ')
      const issuePayload = {
        projectId: project.id,
        issueTypeId: taskIssueType.id,
        name,
      }
      const issue = await jiraClient.createIssue(issuePayload.projectId, issuePayload.name, issuePayload.issueTypeId)
      const issueKey = issue.key
      const url = `${JIRA_HOST}/browse/${issueKey}`
      const responseText = `${getRandomResponse('create-issue')} ${url}`
      bot.replyInThread(message, {
        text: responseText,
      })
      console.log(responseText)
    }
    catch (e) {
      console.error(e)
      bot.replyInThread(message, {
        text: getErrorReply(e),
      })
    }
  })
  controller.hears(['^what time is it?'], ['direct_mention', 'ambient'], async(bot, message) => {
    bot.reply(message, {
      text: `It's shot o'clock https://www.youtube.com/watch?v=QVw5mnRI8Zw`,
    })
  })

  controller.hears(['^wow'], ['direct_mention', 'ambient'], async(bot, message) => {
    bot.reply(message, {
      text: `https://www.youtube.com/watch?v=p9FAx-GnZ6w https://i.ytimg.com/vi/KlLMlJ2tDkg/maxresdefault.jpg`,
    })
  })

  Object.keys(sayings).forEach(key => {
    const value = sayings[key]
    controller.hears([`^${key}`], ['direct_mention', 'ambient'], async(bot, message) => {
      bot.reply(message, {
        text: value,
      })
    })
  })
}

const handleSaveComment = async (controller) => {
  controller.hears(['^save-com'], ['direct_mention', 'ambient'], async(bot, message) => {
    try {
      bot.replyAcknowledge()
      const thread = new Thread(bot, message)
      const replies = await thread.getReplies()
      const rollbackNumber = message.text.split(' ')[2] || 1
      const num = parseInt(rollbackNumber)
      const reply = replies[replies.length - 1 - num]

      const text = await prepareJiraText(bot, message, [reply])

      const issueKey = message.text.split(' ')[1]
      await jiraClient.addCommentToIssue(issueKey, text)

      const url = `${JIRA_HOST}/browse/${issueKey}`
      const responseText = `${getRandomResponse('save-comment')} ${url}`

      bot.replyInThread(message, {
        text: responseText,
      })
      console.log(responseText)
    }
    catch (e) {
      console.log(e)
      bot.replyInThread(message, {
        text: getErrorReply(e),
      })
    }
  })
}


const handleSaveThread = async (controller) => {
  controller.hears(['^save-thread'], ['direct_mention', 'ambient'], async(bot, message) => {
    try {
      bot.replyAcknowledge()
      const thread = new Thread(bot, message)
      const replies = await thread.getReplies()
      replies.pop()
      const text = await prepareJiraText(bot, message, replies)

      const issueKey = message.text.split(' ')[1]
      await jiraClient.appendToIssueDescription(issueKey, text)

      const url = `${JIRA_HOST}/browse/${issueKey}`
      const responseText = `${getRandomResponse('save-thread')} ${url}`

      bot.replyInThread(message, {
        text: responseText,
      })
      console.log(responseText)
    }
    catch (e) {
      console.log(e)
      bot.replyInThread(message, {
        text: getErrorReply(e),
      })
    }
  })
}

const prepareJiraText = async (bot, message, replies) => {
  const authors = replies.map(r => r.user)
  const anyMentionRegex = /<@([A-Z0-9]*)>/g
  const mentions = replies.map(r => {
    const result = []

    let match = anyMentionRegex.exec(r.text)
    while (match != null) {
      result.push(match[1])
      match = anyMentionRegex.exec(r.text)
    }
    return result
  })
    .reduce((prev, current) => {
      if (!current) {
        return prev
      }
      return prev.concat(current)
    }, [])

  const userIds = Array.from(new Set(authors.concat(mentions)))

  const userPromises = userIds.map(id => slackStore.getUser(bot, id))
  const users = await Promise.all(userPromises)

  const injectUsernames = text => {
    let result = text
    users.forEach(user => {
      const regex = new RegExp(`<@${user.id}>`, 'g')
      const groups = result.match(regex)
      if (groups) {
        result = result.replace(regex, '@' + user.name)
      }
    })
    return result
  }

  const simpleMessages = replies.map(r => ({
    username: users.find(u => u.id === r.user).name,
    text: injectUsernames(r.text),
  }))
  const text = simpleMessages.map(m => `${m.username}: ${m.text}`).join('\n\n\n')

  return text
}

const commands = {
  '/issue': handleIssueCommand,
}

const handleSlashCommands = (controller) => {
  controller.on('slash_command', (bot, message) => {
    const func = commands[message.command]
    if (func) {
      func(bot, message)
    }
    else {
      console.error(`slash command not found: ${message.command}`)
    }
  })
}

const run = async(controller) => {
  handleSaveThread(controller)
  handleIssueCommand(controller)
  handleSaveComment(controller)
}

const listChannels = (bot) => {
  bot.api.conversations.list({debug: false}, (err, res) => {
    console.log(res.channels.map(({id, name}) => ({id, name})))
  })
}

module.exports = {
  run,
}
