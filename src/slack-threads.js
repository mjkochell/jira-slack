const repliesApi = ({})

class Thread {
  constructor(bot, message) {
    this.bot = bot
    this.message = message
  }

  async getReplies() {
    const cursor = ''
    return await this.fetchReplySections(cursor)
  }

  async getLastReply() {
    const replies = await this.getReplies()
    return replies[replies.length - 2]
  }

  async fetchReplySections(cursor) {
    return new Promise(async(resolve, reject) => {
      const token = process.env.AUTH_TOKEN
      const {thread_ts: ts, channel} = this.message
      this.bot.api.conversations.replies({ts, channel, cursor, token}, async(bot, res) => {
        let messages
        if (res.error) {
          reject(res)
          return
        }
        if (res.has_more) {
          const cursor = res.response_metadata.next_cursor
          const otherMessages = await(this.fetchReplySections(cursor))
          messages = res.messages.concat(otherMessages)
        }
        else {
          messages = res.messages
        }
        resolve(messages)
      })
    })
  }
}

module.exports = Thread
