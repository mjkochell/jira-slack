const moment = require('moment')
const JiraClient = require('./jira')
const jiraProjectInfo = require('../config/jira')
const {JIRA_HOST, JIRA_USER, JIRA_PASSWORD, JIRA_PROJECT} = process.env

const project = jiraProjectInfo.projects.find(p => p.name === JIRA_PROJECT)
const bugIssueType = jiraProjectInfo.issueTypes.find(p => p.name === 'Bug')
const taskIssueType = jiraProjectInfo.issueTypes.find(p => p.name === 'Task')

const config = {
  host: JIRA_HOST,
  user: JIRA_USER,
  password: JIRA_PASSWORD,
}

const jiraClient = new JiraClient(config)

var slack = {
  async replyIssue(bot, message, options = {title: 'Create Issue', summary: ''}) {
    // bot.replyAcknowledge()
    const issuePayload = {
      projectId: project.id,
      name: message.text,
      issueTypeId: taskIssueType.id,
    }
    const issue = await jiraClient.createIssue(issuePayload.projectId, issuePayload.name, issuePayload.issueTypeId)
    const issueKey = issue.key
    const url = `${JIRA_HOST}/browse/${issueKey}`
    const responseText = `Issue Created: ${message.text} \n${url}`
    bot.replyPublic(message, {
      text: responseText,
    })
  },
  replyIssueDialog(bot, message, options = {title: 'Create Issue', summary: ''}) {
    bot.replyAcknowledge()

    // var dialog = bot.createDialog(
    //   'Submit an issue',
    //   'create-issue',
    //   // 'bug-report',
    //   'Submit'
    // ).addText('Title','title', options.title)
    //   .addSelect('Issue Type', 'issueTypeId', taskIssueType.id, [
    //     {label: 'Task', value: taskIssueType.id},
    //     {label: 'Bug', value: bugIssueType.id},
    //   ])
    //   .addSelect('Project', 'projectId', irisProject.id,[
    //     {label: 'Iris', value: irisProject.id},
    //     {label: 'Api Test', value: apiTestProject.id},
    //   ])
    //   .addTextarea('Summary', 'summary', options.summary, {placeholder: 'Explain the issue here'})

    // bot.replyWithDialog(message, dialog.asObject())
  }

}

module.exports = slack
