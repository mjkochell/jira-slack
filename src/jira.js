/*

/jira-make-issue "User can't do the thing" "Bugs"
checks if that epic exists first, if provided
*/
const jiraInfo = require('../config/jira')
const {JIRA_HOST, JIRA_USER, JIRA_PASSWORD} = process.env

const DEFAULT_CONFIG = {
  host: JIRA_HOST,
  user: JIRA_USER,
  password: JIRA_PASSWORD,
}
const JiraClient = require('jira-connector')

const arrToObj = arr => arr.reduce((prev, current) => {
  prev[current.name] = current
  return prev
}, {})

class Client {

  constructor(config=DEFAULT_CONFIG) {
    this.config = config
    this.info = {
      projects: arrToObj(jiraInfo.projects),
      issueTypes: arrToObj(jiraInfo.issueTypes),
    }
    this.jira = new JiraClient({
      host: config.host,
      basic_auth: {
        username: config.user,
        password: config.password,
      }
    })
  }

  createIssue(projectId, name, issueTypeId=this.info.issuesTypes.taskIssueType.id, epicId='') {
    return new Promise((resolve, reject) => {
      const newIssue = {
        fields: {
          project: {
            id: projectId,
          },
          summary: name,
          issuetype: {
            id: issueTypeId,
          },
        },
      }
      this.jira.issue.createIssue(newIssue, function(err, issue) {
        if (err) {
          console.log(err)
          return reject(err)
        }
        resolve(issue)
      })
    })
  }

  async addCommentToIssue(issueKey, comment) {
    const issue = await this.getIssue(issueKey)
    return new Promise((resolve, reject) => {
      this.jira.issue.addComment({issueKey, comment}, (err) => {
        if (err) {
          reject(err)
          return
        }
        resolve()
      })
    })
  }

  async appendToIssueDescription(issueKey, description) {
    const issue = await this.getIssue(issueKey)
    console.log(issue.fields.description)
    const newDescription = `${issue.fields.description || ''}\n\n${description || ''}`
    console.log(newDescription)
    const res = await this.updateIssue(issueKey, {description: newDescription})
    console.log(res)
  }

  async updateIssue(issueKey, fields) {
    return new Promise((resolve, reject) => {
      this.jira.issue.editIssue({issueKey, issue: {fields}}, (err, issue) => {
        if (err) {
          console.log(err)
          return reject(err)
        }
        resolve(issue)
        // console.log(issue.fields.summary);
      })
    })
  }

  async getIssue(issueKey=`${JIRA_PROJECT}-1`) {
    return new Promise((resolve, reject) => {
      this.jira.issue.getIssue({issueKey}, (err, issue) => {
        if (err) {
          console.log(err)
          return reject(err)
        }
        resolve(issue)
        // console.log(issue.fields.summary);
      })
    })
  }
}

module.exports = Client
