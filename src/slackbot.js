var Botkit = require('botkit')
var redis = require('botkit-storage-redis')
var url = require('url')

require('dotenv').config()

const scopes = [
  'bot',
]

var botOptions = {
  clientId: process.env.CLIENT_ID,
  clientSecret: process.env.CLIENT_SECRET,
  clientSigningSecret: process.env.SIGNING_SECRET,
  scopes: scopes,
  debug: process.env.DEBUG || false
}

if (process.env.REDIS_URL) {
  var redisURL = url.parse(process.env.REDIS_URL)
  botOptions.storage = redis({
    namespace: process.env.REDIS_NAMESPACE,
    host: redisURL.hostname,
    port: redisURL.port,
    auth_pass: redisURL.auth ? redisURL.auth.split(":")[1] : null,
  })
} else {
  botOptions.json_file_store = __dirname + '/.data/db/' // store user data in a simple JSON format
}

if (!process.env.BOT_TOKEN) {
  console.log('Error: Specify BOT_TOKEN in environment')
  process.exit(1)
}

let bot
let controller

const startRTM = () => {
  return new Promise((resolve, reject) => {
    bot.startRTM(async(err, res) => {
      if (err) {
        setTimeout(startRTM, 60000);
        reject(err)
      }
      resolve({bot, controller, res})
    })
  })
}

const init = async () => {
  controller = Botkit.slackbot(botOptions)
  bot = controller.spawn({
    debug: false,
    token: process.env.BOT_TOKEN,
  })

  createSlashCommandWebServer(controller)

  controller.on('rtm_close', function(bot, err) {
    startRTM()
  })

  return startRTM()
}

const createSlashCommandWebServer = controller => {
  controller.setupWebserver(process.env.PORT,function(err,webserver) {
    controller.createWebhookEndpoints(controller.webserver)
    controller.createOauthEndpoints(controller.webserver, function(err, req, res) {
      if (err) {
        res.status(500).send('ERROR: ' + err)
      } else {
        res.send('Success!')
      }
    })
    webserver.get('/', function(req, res) {
      res.send('OK');
    })
  })
}

module.exports = {init}
