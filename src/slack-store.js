const users = {

}

const channels = {

}

const getUser = async(bot, userId) => {
  if(users[userId]) {
    return users[userId]
  }
  return new Promise((resolve, reject) => {
    bot.api.users.info({user: userId}, (bot, res) => {
      users[userId] = res.user
      resolve(res.user)
    })
  })
}

module.exports = {
  getUser,
}
